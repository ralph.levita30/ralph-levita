import React from 'react';

const UserContext = React.createContext();

//provider
export const UserProvider = UserContext.Provider;

export default UserContext;
